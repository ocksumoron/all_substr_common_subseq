//
//  lcs.h
//  all_substr_common_subseq
//
//  Created by Даня on 27.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#ifndef all_substr_common_subseq_lcs_h
#define all_substr_common_subseq_lcs_h

#include <string>
#include <vector>

typedef unsigned int ui;

using std::vector;
using std::string;
using std::max;
using std::min;

vector <vector <bool> > getCriticalPoints(string &mainString, string &subseqString) {
    vector <vector <bool> > result;
    vector <vector <int> > horisontalDynamics;
    vector <vector <int> > verticalDynamics;
    result.resize(subseqString.size() + 1);
    horisontalDynamics.resize(mainString.size() + 1);
    verticalDynamics.resize(mainString.size() + 1);
    for (ui i = 0; i <= subseqString.size(); ++i) {
        result[i].resize(subseqString.size() + 1, 0);
    }
    for (ui i = 0; i <= mainString.size(); ++i) {
        horisontalDynamics[i].resize(subseqString.size() + 1, 0);
        verticalDynamics[i].resize(subseqString.size() + 1, 0);
    }
    for (ui i = 0; i <= subseqString.size(); ++i) {
        horisontalDynamics[0][i] = i;
    }
    for (ui i = 0; i <= mainString.size(); ++i) {
        verticalDynamics[i][0] = 0;
    }
    for (ui i = 1; i <= mainString.size(); ++i) {
        for (ui j = 1; j <= subseqString.size(); ++j) {
            if (mainString[i - 1] == subseqString[j - 1]) {
                horisontalDynamics[i][j] = verticalDynamics[i][j - 1];
                verticalDynamics[i][j] = horisontalDynamics[i - 1][j];
            } else {
                horisontalDynamics[i][j] = max(horisontalDynamics[i - 1][j], verticalDynamics[i][j - 1]);
                verticalDynamics[i][j] = min(horisontalDynamics[i - 1][j], verticalDynamics[i][j - 1]);
            }
        }
    }
    for (ui i = 1; i <= subseqString.size(); ++i) {
        if (horisontalDynamics[mainString.size()][i]) {
            result[horisontalDynamics[mainString.size()][i] - 1][i] = 1;
        }
    }
    return result;
}

vector <vector <int> > getFastMatrixLCS(string &mainString, string &subseqString) {
    vector <vector <int> > result;
    vector <vector <bool> > criticalPoints = getCriticalPoints(mainString, subseqString);
    result.resize(criticalPoints.size());
    for (ui i = 0; i < result.size(); ++i) {
        result[i].resize(criticalPoints.size(), 0);
    }
    for (ui j = 1; j < result.size(); ++j) {
        for (ui i = 0; i < result.size() - j; ++i) { //result[i][i + j]
            ui curX = i;
            ui curY = i + j;
            if (result[curX][curY - 1] != result[curX + 1][curY]) {
                result[curX][curY] = max(result[curX][curY - 1], result[curX + 1][curY]);
            } else {
                if (j == 1 || result[curX + 1][curY - 1] != result[curX + 1][curY]) {
                    result[curX][curY] = result[curX + 1][curY] + 1 - criticalPoints[curX][curY];
                } else {
                    result[curX][curY] = result[curX + 1][curY];
                }
            }
        }
    }
    return result;
}



#endif
