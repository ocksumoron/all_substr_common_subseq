//
//  tests.h
//  all_substr_common_subseq
//
//  Created by Даня on 27.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#ifndef all_substr_common_subseq_tests_h
#define all_substr_common_subseq_tests_h

#include "lcs.h"
#include "stupid.h"
#include <string>
#include <random>
#include <algorithm>

using std::next_permutation;

typedef unsigned int ui;

int generateRandom(int minX, int maxX)
{
    static std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(minX, maxX);
    return distribution(generator);
}

string generateString(ui size) {
    string result = "";
    for (ui i = 0; i < size; ++i) {
        result += char(generateRandom(0, 25) + 'a');
    }
    return result;
}

bool proceedTest(ui sizeMain, ui sizeSubseq, ui testSize) {
    string mainString = generateString(sizeMain);
    string subseqString = generateString(sizeSubseq);
    string currentSubseqString = subseqString;
    vector <ui> subseqArr;
    subseqArr.resize(sizeSubseq + 1);
    int i = 0;
    bool flag = true;
    for (ui i = 0; i < sizeSubseq; ++i) {
        subseqArr[i] = i;
    }
    do {
        for (ui i = 0; i < sizeSubseq; ++i) {
            currentSubseqString[i] = subseqString[subseqArr[i]];
        }
        flag &= getFastMatrixLCS(mainString, currentSubseqString) == getSlowMatrixLCS(mainString, currentSubseqString);
        i++;
    } while (i < testSize && next_permutation(subseqArr.begin(), subseqArr.end()));
    return flag;
}

#endif
