//
//  main.cpp
//  all_substr_common_subseq
//
//  Created by Даня on 27.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#include <iostream>
#include "tests.h"

using std::cin;
using std::cout;

int main() {
//    string a, b;
//    cin >> a >> b;
//    vector <vector <int> > res = getFastMatrixLCS(a, b);
//    for (int i = 0; i < res.size(); ++i) {
//        for (int j = 0; j < res[i].size(); ++j) {
//            if (i <= j)
//                cout << res[i][j] << " ";
//            else
//                cout << "  ";
////            cout << (i >= j ? res[i][j] : " ");
////            cout << (res[i][j] ? 1 : 0) << " ";
//        }
//        cout << "\n";
//    }
//    cout << "lol\n";
//    res = getSlowMatrixLCS(a, b);
//    for (int i = 0; i < res.size(); ++i) {
//        for (int j = 0; j < res[i].size(); ++j) {
//            if (i <= j)
//                cout << res[i][j] << " ";
//            else
//                cout << "  ";
//            //            cout << (i >= j ? res[i][j] : " ");
//            //            cout << (res[i][j] ? 1 : 0) << " ";
//        }
//        cout << "\n";
//    }
//    cout << "lol";
    int mainStringSize, subseqStringSize, testSize;
    cin >> mainStringSize >> subseqStringSize >> testSize;
    cout << (proceedTest(mainStringSize, subseqStringSize, testSize) ? "CORRECT" : "FAILED");
    return 0;
}

//yxxyzyzx
//yxxyzxyzxyxzx
