//
//  stupid.h
//  all_substr_common_subseq
//
//  Created by Даня on 27.04.15.
//  Copyright (c) 2015 mipt. All rights reserved.
//

#ifndef all_substr_common_subseq_stupid_h
#define all_substr_common_subseq_stupid_h

#include <string>
#include <vector>

using std::vector;

vector <vector <int> > getSlowMatrixLCS(string &mainString, string &subseqString) {
    vector <vector <int> > result;
    vector <vector <int> > dynamics;
    result.resize(subseqString.size() + 1);
    for (ui i = 0; i < result.size(); ++i) {
        result[i].resize(subseqString.size() + 1, 0);
    }
    dynamics.resize(mainString.size() + 1);
    for (ui i = 0; i < dynamics.size(); ++i) {
        dynamics[i].resize(subseqString.size() + 1);
    }
    for (ui i = 1; i <= subseqString.size(); ++i) {
        for (ui j = 0; j < dynamics.size(); ++j) {
            for (ui k = 0; k < dynamics[j].size(); ++k) {
                dynamics[j][k] = 0;
            }
        }
        for (ui j = 1; j <= mainString.size(); ++j) {
            for (ui k = i; k <= subseqString.size(); ++k) {
                dynamics[j][k] = max(dynamics[j - 1][k], dynamics[j][k - 1]);
                if (mainString[j - 1] == subseqString[k - 1]) {
                    dynamics[j][k] = max(dynamics[j][k], dynamics[j - 1][k - 1] + 1);
                }
            }
        }
        for (ui j = i; j <= subseqString.size(); ++j) {
            result[i - 1][j] = dynamics[mainString.size()][j];
        }
    }
    return result;
}


#endif
